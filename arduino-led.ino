void setup() {
  // put your setup code here, to run once:
  // Inicialización del conector 13: conector del LED  
  pinMode(13, OUTPUT); 
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(13, HIGH);  // apaga la luz del LED  
  delay(1000); // espera un segundo  
  digitalWrite(13, LOW); // enciende la luz del LED  
  delay(1000); // espera un segundo  

}
